## ZigBee Stack Research

![Scheme](images/1.png)

![Scheme](images/2.png)

Zigbee'nin üzerine kurulduğu temel olarak iki katmandan oluşur. Onlar:
•	Temel Katmanları
•	Uygulama ve Arayüz Bölümü

### 1.	Temel Katmanları
Bu Katman, IEEE 802.15.4 standardı tarafından tanımlanmıştır. Hem Fiziksel katman hem de Orta Erişim Kontrolü (MAC) katmanları, bu teknoloji için temel katmanlar olarak işlev görür.
Fiziksel ve elektriksel özellikler fiziksel katman tarafından tanımlanır. Orta Erişim Kontrolü (MAC) katmanı ise fiziksel ve ağ katmanları arasında arayüz sağlar.

### 1.1.	Fiziksel Katman
Fiziksel ve elektriksel özellikler Fiziksel Katman tarafından tanımlanır. Bu katman, veri iletimi ve alımından sorumludur. Fiziksel katmanın temel görevi olan modülasyon ve yayma teknikleri ile bilgi bitlerini haritalamak ve havada seyahat etmelerini sağlar.

#### 1.1.1.	Zigbee Mimarisinde Fiziksel Katmanın İşlevi
Fiziksel Katman aşağıdaki işlevlerden sorumludur:
•	İletim ve alımın etkinleştirilmesi ve devre dışı bırakılması.
•	Kanal seçimi ve değerlendirilmesi.
•	Paketlerin gönderilmesi ve alınması.
•	Kanal içinde enerji algılama.

### 1.2.	Orta Erişim Kontrolü (MAC) Katmanı
Bu katman, fiziksel ve ağ katmanları arasında arayüz sağlar. Aynı alanda çalışan birden fazla 802.15.4 telsizin hava dalgalarını nasıl paylaşacağını tanımlar. Veri işleme ve veri yönetimi, MAC katmanının iki ana işlevidir.
Veri işleme, Veri Talebi ve Veri Onayı gibi işlevleri içerir. MAC katmanı, hedef adresi ekler ve giden veri çerçeveleri için seçenekler iletir.
Zigbee ağ katmanı “veri talebi” işlevini çağırdığında, veriler ilgili MAC başlığına formatlanır ve fiziksel başlık olan çerçeve uzunluğu eklenir. Veri çerçevesi iletilmeye hazır olur.
“Veri Onayı” işlevinin amacı, iletilen verilerin durumunu iletmektir. İletim çerçeveleri aşıldığında veya iletilen verilere yanıt olmadığında bir arıza durumu gönderir.

#### 1.2.1.	 Zigbee Mimarisinde Ortam Erişim Kontrolü (MAC) Katmanının İşlevi
Orta Erişim Kontrolü (MAC) Katmanı aşağıdakilerden sorumludur:
•	Beacon üretimi ve yönetimi.
•	CSMA-CA (Çarpışma Önlemeli Carrier Sense Çoklu Erişim) uygulanmaktadır.
•	Garantili Zaman Pıhtısı yönetimi (GTS).
•	Veri çerçevesi doğrulaması ve onayı.
•	Üst katmanlar için veri aktarımı.

#### 2.	Uygulama ve Arayüz Bölümü
Bu bölüm Zigbee Spesifikasyonları tarafından tanımlanır ve Ağ Katmanı ile Uygulama Katmanını içerir. Ağ Katmanı, MAC katmanı ile uygulama katmanı arasında arayüz sağlar. Star, Mesh ve Tree topolojisi gibi farklı ağ topolojilerinin yönlendirilmesinden ve kurulmasından sorumludur. Bir ağın başlatılması, düğüm adreslerinin atanması, yeni cihazların yapılandırılması, güvenli iletimin sağlanması ağ katmanının sorumluluğundadır.
Uygulama Katmanı, Uygulama Destek Alt Katmanı ve Uygulama Çerçevesi olmak üzere alt katmanlardan oluşur.
Uygulama Desteği Alt Katmanı (APS), paketlerin uç cihazlar için filtrelenmesinden sorumludur, otomatik yeniden denemeleri destekleyen bir ağda yaygın olan paketlerin tekrarını kontrol eder.
Uygulama Çerçevesi, söz konusu satıcı için uç noktaların nasıl uygulandığını, veri isteklerinin ve veri onayının nasıl yürütüldüğünü temsil eder.

### 2.1. Ağ Katmanı

![Scheme](images/3.png)

Ağ Katmanı, MAC katmanı ile uygulama katmanı arasında arayüz sağlar. Star, Mesh ve Tree topolojileri gibi farklı Zigbee ağ topolojilerinin yönlendirilmesinden ve kurulmasından sorumludur .
Bir koordinatör bir Zigbee ağı kurmaya çalıştığında, yeni ağı için en iyi RF kanalını bulmak için bir enerji taraması başlatılır. Bir kanal seçildiğinde, koordinatör ağa katılan tüm cihazlara uygulanacak bir PAN-ID atar.
PAN-ID, ağ tanımlayıcısı olarak kullanılan 16 bitlik bir sayıdır. Bir düğümün, yalnızca ilişkilendirme sürecinden geçtiğinde ağ üzerinde iletişim kurmasına izin verilir. İlişkilendirme işlevi, bir düğümü bir ebeveyne bağlamak için kullanılır.
Bir düğüm ebeveynini kaybettiğinde, yetim bir cihaz olarak kabul edilir. Genellikle uç cihaz mobil olduğunda ve menzil dışında olduğunda veya ebeveyndeki bir arıza nedeniyle oluşur. Böyle bir durumda, cihazın ebeveynini bulmasına yardımcı olan bir “yetim bildirimi” komut çerçevesi yayınlanarak bir yetim tarama gerçekleştirilir.
Ebeveyn bildirim komutunu alırsa, var olduğu cihaza geri döner ve yetim ebeveyne yeniden katılabilir. Böylece bir iletimin gerçekliği ve gizliliği ağ katmanı tarafından sağlanır.

#### 2.1.1.	Zigbee Mimarisinde Ağ Katmanının İşlevi
Zigbee mimarisindeki Ağ Katmanı aşağıdaki işlevlerden sorumludur:
•	Bir ağın başlatılması
•	Düğüm adresleri atama
•	Yeni cihazların yapılandırılması
•	Güvenli iletim sağlamak

### 2.2.	Uygulama Katmanı
Zigbee mimarisindeki Uygulama Katmanı, aşağıdaki alt katmanlardan oluşur:
•	Uygulama Desteği Alt Katmanı
•	Uygulama Çerçevesi

#### 2.2.1.	Uygulama Desteği Alt Katmanı (APS)
Bu katman, paketlerin uç cihazlar için filtrelenmesinden sorumludur, otomatik yeniden denemeleri destekleyen bir ağda yaygın olan paketlerin tekrarını kontrol eder. Başarılı iletim şansını en üst düzeye çıkarmak için, gönderen tarafından onay istendiğinde otomatik yeniden denemeler gerçekleştirir.
Bağlama tablolarının korunmasında yer alır. Bağlama, düğümdeki uç nokta ile diğer düğümlerdeki bir veya daha fazla uç nokta arasındaki bağlantıdır. Adres eşleme, 64 bit MAC adresini Zigbee 16 bit ağ adresiyle ilişkilendirir.

### 2.2.1.1.	Uygulama Desteği Alt Katmanı (APS) aşağıdaki işlevlerden sorumludur:
•	Bağlama tablolarının bakımı.
•	Adres tanımlama, haritalama ve yönetimi.
•	Cihazlar arası iletişimi sağlamak.
•	Kayıtlı olmayan uç cihazlar veya eşleşmeyen profiller için paketlerin filtrelenmesi ve paketlerin yeniden birleştirilmesi.

### 2.3.	Uygulama Çerçevesi
Uygulama Çerçevesi, belirli uygulamaların Zigbee protokolüyle etkileşime girmesini seçen satıcıya bağlıdır. Bu, belirli satıcı için uç noktaların nasıl uygulandığını, veri isteklerinin ve veri onayının nasıl yürütüldüğünü temsil eder.


